﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NunitConsoleApp
{
    public class Calculator
    {
        public int Multiply(int digit1, int digit2)
        {
            return digit1 * digit2;
        }
        public int Add(int digit1, int digit2)
        {
            return digit1 + digit2;
        }
        public int Subtract(int digit1, int digit2)
        {
            return digit1 - digit2;
        }
        public int Divide(int digit1, int digit2)
        {
            return digit1 / digit2;
        }
    }
}
