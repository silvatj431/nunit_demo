﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace NunitConsoleApp.Test
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void MultiplyIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Multiply(9, 3);

            Assert.AreEqual(27, result);
        }
        [Test]
        public void AddIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Add(9, 3);

            Assert.AreEqual(12, result);
        }
        [Test]
        public void SubtractIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Subtract(9, 3);

            Assert.AreEqual(6, result);
        }
        [Test]
        public void DivideIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Divide(9, 3);

            Assert.AreEqual(3, result);
        }
        [Test]
        public void MultiplyIsNotCorrect()
        {
            var calc = new Calculator();
            var result = calc.Multiply(9, 3);

            Assert.AreNotEqual(28, result);
        }
        [Ignore]
        [Test]
        public void MultiplyIsFailing()
        {
            var calc = new Calculator();
            var result = calc.Multiply(9, 3);

            Assert.AreEqual(28, result);
        }
        [TestCase(6,4, Result = 24)]
        [TestCase(7,4, Result = 28)]
        [TestCase(9,3, Result = 27)]
        public int MultiplyGoodValues(int d1, int d2)
        {
            var calc = new Calculator();
            var result = calc.Multiply(d1, d2);

            return result;
        }
    }
}
